#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define ALFASIZE 26
#define MAXWORDS 1024
#define WORDLENGTH 64
char words[MAXWORDS][WORDLENGTH];
int results[ALFASIZE];

typedef void * obj;
typedef struct no { obj o; struct no *next;	}no;
typedef struct list{ int size; no *first; }list;
typedef struct trie{ char c; int v; list *childTrie; int end; }trie;
trie *t;

no *createNo(obj o) {
  no *n = (no *) malloc(sizeof(no));
  n->o = o; n->next = NULL;
  return n;
}

list *createList() {
  list *l = (list *) malloc(sizeof(list));
  l->size = 0; l->first = NULL;
  return l;
}

void freeList(list *l) {
  no *n = l->first;
  no *aux = l->first;
  while(n) { aux = n->next; free(n); n = aux; }
  free(l);
}

void insertList(list *l, obj o) {
  if (l->size == 0) l->first = createNo(o);
  else {
    no * aux = l->first;
    while(aux->next) aux = aux->next;
    aux->next = createNo(o); 
  }
  l->size++;
}

trie *createTrie() {
  trie *t = (trie *) malloc(sizeof(trie));
  t->c = '\n'; t->v = 0; t->childTrie = createList(); t->end = 0;
  return t;
}

trie *trieChildThatContains(trie *a, char current) {
  trie *res = NULL;
  trie *tCurr;
  no *aux = a->childTrie->first;
  while(aux) {
    tCurr = (trie*) aux->o;
    if (tCurr->c == current) res = tCurr;
    aux = aux->next;
  }
  return res;
}

int f(trie *triePointer, int root) {
  int r = 0;
  if(triePointer->childTrie->size == 0) { triePointer->v = 1;  }
  no *aux = triePointer->childTrie->first;
  while(aux) {
    r += f((trie*)aux->o, 0);
    aux = aux->next;
  }
  if(triePointer->end) r = r + 1; 
  triePointer->v = r;
  if(triePointer->v >= 2) r = r - 2;
  triePointer->v = r;
  return r;
}

void fv(trie *triePointer, int *val, int N) { *val = *val + triePointer->v; }

void showTrie(trie *t, int count) {
  printf("%c(%d)(%d)(%d) | ", t->c, count, t->v, t->end);
  no *aux = t->childTrie->first;
  if(!aux) printf("\n");
  while(aux) {
    showTrie((trie*)aux->o, count+1);
    aux = aux->next;
  }
}

void freeTrie(trie *t) {
  list *childs = t->childTrie;
  no *n = childs->first;
  while(n){ freeTrie(n->o); n = n->next; }
  freeList(childs); free(t);
}

void populateTrie(trie *t, int wordPosition) {
  trie *aux = t;
  trie *search = NULL;
  trie *newT;
  long int size = strlen(words[wordPosition]);
  char current;
  while(size > 0) {
    current = words[wordPosition][size - 1];
    if (aux->c == '\n'){
      aux->c = current;
      if(size == 1) aux->end = 1;
      else {
        newT = createTrie();
        insertList(aux->childTrie, newT);
        aux = newT;
      }
    }
    else if(aux->c == current && size > 1) {
      search = trieChildThatContains(aux, words[wordPosition][size - 2]);
      if(search) { aux = search; if(size == 2) aux->end=1; }
      else {
        newT = createTrie();
        insertList(aux->childTrie, newT);
        aux = newT;
      }
    }
    else break;
    size--;
  }
}

int result(int N) {
  int r,count; char a;
  int resultsX = 0;
  int resultsSum = 0;
  memset(results, -1, sizeof results);
  for(int j = 0; j < N; j++) {
    a = words[j][strlen(words[j]) - 1] - 65;
    if(results[a] == -1) {
      t = createTrie();
      count = 0;
      for(int i = 0; i < N; i++) {
        if(words[i][strlen(words[i]) - 1] - 65 == a) {
          count++;
          populateTrie(t, i);
        }
      }
      f(t, 1);
      r = 0;
      fv(t, &r, N);
      // printf("printinf Trie\n");
      // showTrie(t, 0);
      if (r > resultsX) resultsX = r;
      resultsSum += t->v;
      results[a] = r;
      freeTrie(t);
    }  
  }
  if (resultsX - resultsSum <= 0) return N - resultsSum;
  return N - resultsX - resultsSum;
}

int main() {
  int T, N;
  scanf("%d", &T);
  for(int k = 1; k <= T; k++) {
    scanf("%d", &N);
    for(int i = 0; i < N; i++) scanf("%s", words[i]);
    printf("Case #%d: %d\n", k, result(N));
  }
  return 0;
}
