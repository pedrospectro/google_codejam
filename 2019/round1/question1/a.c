#include <stdio.h>
#include <string.h>
#define MAXR 21
#define MAXC 21
int galaxy[MAXR][MAXC];

int invalid(int i, int j, int newI, int newJ) {
  return (
    galaxy[newI][newJ] != 0 || i == newI || j == newJ || 
    i - j == newI - newJ || i + j == newI + newJ
  );
}

void findPath(int count, int R, int C, int i, int j, int *possible) {
  if(count == R * C - 1) {
    galaxy[i][j] = count + 1;
    *possible = 1;
    return;
  }
  int previous = galaxy[i][j];
  galaxy[i][j] = count + 1;
  for(int ii = 1; ii <= R ; ii++)
    for(int jj = 1; jj <= C; jj++)
      if(!invalid(i, j, ii, jj))
        findPath(count + 1, R, C, ii, jj, possible);
  if (*possible == 0) galaxy[i][j] = previous;
}

void printPath(int R, int C) {
  int count = 1;
  while(count <= R * C) {
    for(int i = 1; i <= R; i++)
      for(int j = 1; j <= C; j++)
        if(galaxy[i][j] == count) printf("%d %d\n", i, j);
    count++;
  }
}

void result(int R, int C) {
  int tries = 0;
  int possible = 0;
  int startI, startJ;
  startI = startJ = 1;
  while(!possible && tries < (R * C)) {
    memset(galaxy, 0, sizeof galaxy);
    findPath(0, R, C, startI, startJ, &possible);
    if(startJ < C) startJ++; else { startJ = 1; startI++; }
    tries++; 
  }
  printf((possible == 0) ? "IMPOSSIBLE\n" : "POSSIBLE\n");
  if(possible) printPath(R, C);
}

int main() {
  int count, T, R, C;
  scanf("%d", &T);
  for(count = 1; count <= T; count++) {
    scanf("%d %d", &R, &C);
    printf("Case #%d: ", count);
    result(R, C);
  }
  return 0;
}
