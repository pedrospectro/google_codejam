#include <stdio.h>
#include <string.h>
#define MAXDAYS 365
#define HOLESLENGTH 18

int holeBlades[HOLESLENGTH];
int bladeRegister[MAXDAYS][HOLESLENGTH];

void result(int N, int M) {
  int i, j;
  memset(bladeRegister, 0, sizeof bladeRegister);
  for(i = 0; i < N+1; i++) {
    memset(holeBlades, i, sizeof holeBlades);
    for(j = 0; j < HOLESLENGTH; j++) printf("%d ", holeBlades[j]);
    printf("\n"); fflush(stdout);
    for(j = 0; j < HOLESLENGTH; j++) scanf("%d", &bladeRegister[i][j]);
  }
  printf("%d\n", HOLESLENGTH); fflush(stdout);
}

int main() {
  int count, T, N, M, judge;
  scanf("%d %d %d", &T, &N, &M);
  for(count = 1; count <= T; count++) {
    result(N, M); scanf("%d", &judge);
  }
  fflush(stdout); fflush(stdin);
  printf("\n"); fflush(stdout);
  return 0;
}
