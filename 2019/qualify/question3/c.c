#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define MAXL 102

char *alfa;
int alfaSize;

/*Greast Common Divisor*/
int gcd(int a, int b) {
  if (b == 0) return a;
  if (b > a) return gcd(b, a);
  else return gcd(b, a % b);
}

int findIndex(int *array, int value, int size) {
  for(int i = 0; i < size; i++) if(array[i] == value) return i;
  return -1;
}

void sortArray(int *array, int size) {
  int aux;
  for(int i = 0; i < size; i++) {
    for(int j = (i + 1); j < size; j++) {
      if(array[j] < array[i]) {
        aux = array[i];
        array[i] = array[j];
        array[j] = aux;
      }
    }  
  }
}

int * getSortedUniquePrimes(int *array, int size, int *primesSize) {
  int loopPrimesSize = 0;
  int *loopPrimes = (int *) calloc(size, sizeof(int));
  for(int i = 0; i < size; i++)
    if(findIndex(loopPrimes, array[i], size) == -1) loopPrimes[loopPrimesSize++] = array[i];
  sortArray(loopPrimes, loopPrimesSize);
  *primesSize = loopPrimesSize;
  return loopPrimes;
}

void result(int N, int L, int encodedText[]) {
  int i, j, a, b, p, q, loopPrimesSize, factoredSize, factoredReverseSize;
  int *numsCopy = (int*) malloc(L * sizeof(int));
  int *nums = (int*) malloc(L * sizeof(int));
  int *numsRemoved = (int*) malloc(L * sizeof(int));
  int *factored = (int*) malloc((L + L + 2) * sizeof(int));
  int *factoredReverse = (int*) malloc((L + L + 2) * sizeof(int));
  for(i = 0; i < L; i++) numsCopy[i] = encodedText[i];
  i = 0;
  while(i < (L - 1) && (encodedText[i] == encodedText[i + 1])) i++;
  int numsSize = L - i; int numsRemovedSize = i;
  for(j = i; j < L; j++) nums[j - i] = encodedText[j];
  for(j = 0; j < numsRemovedSize; j++) numsRemoved[j] = encodedText[j];
  a = nums[0]; b = nums[1];
  p = gcd(a, b); q = floor(a / p);
  factored[0] = q; factored[1] = p; factored[2] = floor(b / p);
  factoredSize = 3; p = factored[2];
  for(i = 2; i < numsSize; i++) { p = floor(nums[i] / p); factored[factoredSize++] = p;
  }
  factoredReverseSize = 0;
  for(i = (factoredSize - 1); i >= 0; i--) factoredReverse[factoredReverseSize++] = factored[i];
  for(i = 0; i < numsRemovedSize; i++) {
    q = floor(numsRemoved[i] / q); factoredReverse[factoredReverseSize++] = q;
  }
  for(i = 0; i < factoredReverseSize; i++) factored[i] = factoredReverse[factoredReverseSize - i - 1];
  int * loopPrimes = getSortedUniquePrimes(factored, factoredReverseSize, &loopPrimesSize);
  for(i = 0; i < factoredReverseSize; i++) printf("%c", alfa[findIndex(loopPrimes, factored[i], loopPrimesSize)]);
  printf("\n");
  free(loopPrimes); free(factoredReverse); free(factored); free(numsRemoved); free(numsCopy); free(nums);
}

int main() {
  int i, T, N, L, encodedText[MAXL];
  alfa = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  alfaSize = 26;
  scanf("%d", &T);
  for(int k = 1; k <= T; k++) {
    scanf("%d %d", &N, &L);
    for(i = 0; i < L; i++) scanf("%d", &encodedText[i]);
    printf("Case #%d: ", k);
    result(N, L, encodedText);
  }
  return 0;
}
