#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#define MAX_F 11
#define MAX_RESPONSE_SIZE 1025
int working[MAX_RESPONSE_SIZE];
int notWorking[MAX_RESPONSE_SIZE];
char response[MAX_F][MAX_RESPONSE_SIZE];
char sendBits[MAX_RESPONSE_SIZE];

int binaryPotGreaterEqual(int B, int N) {
  int pot = 1;
  while(pot < N) pot = pot << 1;
  return pot >> 1;
}

int findArray(int value, int array[], int size) {
  for(int i = 0; i < size; i++) if (array[i] == value) return i;
  return -1;
}

int sendData(int i, int N, int B, int pot) {
  memset(sendBits, 0, sizeof sendBits);
  char d = '0';
  int groupSize = pot / pow(2, i);
  if(groupSize <= 1) groupSize = 1;
  for(int i = 0; i < N; i++) {
    if ( i > 0 && (i) % groupSize == 0) { if(d == '0') d = '1'; else d = '0'; }
    sendBits[i] = d;
  }
  printf("%s\n", sendBits);
  fflush(stdout);
  return (groupSize > 1);
}

void result(int N, int B, int F) {
  int pot = binaryPotGreaterEqual(B, N);
  int i, j, currentGroupCount, groupCount;
  currentGroupCount = groupCount = 0;
  int responseSize = N - B;
  
  for(i = 0; i < F; i++){
    currentGroupCount = sendData(i, N, B, pot);
    groupCount += currentGroupCount;
    scanf("%s", response[i]);
    if (currentGroupCount == 0) break;
  }

  int count = 0;
  memset(working, 0, sizeof working); memset(notWorking, 0, sizeof notWorking);
  for(i = 0; i <= groupCount; i++)
    for(j = 0; j < responseSize; j++)
      working[j] = working[j] + (response[i][j] - 48) * pow(2, groupCount - i);

  for(i = 0; i < N; i++) {
    if(findArray(i, working, responseSize) == -1) {
      notWorking[count++] = i;
    }
  }
  for(i = 0; i < count; i++) printf("%d ", notWorking[i]); printf("\n");
  fflush(stdout);
}

int main() {
  int T, N, B, F, res;
  scanf("%d", &T);
  for(int k = 1; k <= T; k++) {
    scanf("%d %d %d", &N, &B, &F);
    result(N , B , F);
    scanf("%d", &res);
  }
  fflush(stdout); fflush(stdin); 
  return 0;
}
