#include <stdio.h>
#include <string.h>
#define MAXN 50000

int notFound;
int maze[MAXN][MAXN];
char path[2 * MAXN - 2];
char pathRes[2 * MAXN - 2];
char pathInv[2 * MAXN - 2];

void genereateMaze(char p[], int N) { 
  int i, j;
  for(i = 0; i < N; i++) for(j = 0; j < N; j++) maze[i][j] = 0;
  i = j = 0;
  maze[0][0] = 1;
  for(int count = 0; count < (2 * N - 2); count++)
    (p[count] == 'S') ? (maze[++i][j] = 1) : (maze[i][++j] = 1);
}

void showMaze(int N) {
  printf("\nMAZE:\n");
  for(int i = 0; i < N; i++) {
    for(int j = 0; j < N; j++) printf("%d ", maze[i][j]);
    printf("\n");
  }
}

int overlap(int N) {
  int i;
  int size = 2 * N - 2;
  for (i = 0; i < size && pathRes[i] == path[i]; i++);
  if (i == size) return 1;
  for (i = 0; i < size && pathRes[i] == pathInv[i]; i++);
  if (i == size) return 1;
  int countX, countY, newCountX, newCountY;
  countX = countY = newCountX = newCountY = 0;
  for(i = 0; i < size; i++) {
    if (pathRes[i] == 'S') newCountX++; else newCountY++;
    if(newCountX >= N || newCountY >= N) return 1;
    if(maze[countX][countY] == 1 && maze[newCountX][newCountY] == 1) return 1;
    countX = newCountX; countY = newCountY;
  }
  return 0;
}

void buildPath(int index, int N, char c) {
  pathRes[index] = c;
  if (index == (2 * N - 4)) {
    if(!overlap(N) && notFound) {
      for(int i = 0 ; i < 2 * N - 2; i++) printf("%c", pathRes[i]);
      printf("\n");
      notFound = 0;
    }
    return;
  }
  buildPath(index + 1, N, 'S');
  buildPath(index + 1, N, 'E');
}

void result(int N) {
  for(int i = 0; i < (2 * N - 2); i++) pathRes[i] = (path[i]=='S') ? 'E' : 'S';
  for(int i = 0; i < (2 * N - 2); i++) pathInv[i] = (path[i]=='S') ? 'E' : 'S';
  genereateMaze(path, N);
  notFound = 1;
  buildPath(0, N, pathRes[0]);
}

int main() {
  int i, count, T, N;
  scanf("%d", &T);
  for(count = 1; count <= T; count++) {
    scanf("%d %s", &N, path);
    printf("Case #%d: ", count);
    result(N);
    if (notFound) {
      for(int j = 0 ; j < 2 * N - 2; j++) printf("%c", pathInv[j]);
      printf("\n");
    } 
  }
  return 0;
}
